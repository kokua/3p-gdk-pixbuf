#!/bin/bash

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

TOP="$(dirname "$0")"

PROJECT=gdk-pixbuf
LICENSE=COPYING
VERSION="2.36.11"
SOURCE_DIR="$PROJECT"


if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    autobuild="$(cygpath -u $AUTOBUILD)"
else
    autobuild="$AUTOBUILD"
fi

# load autbuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)"
GLIB_INCLUDE="${stage}"/packages/include/glib
LIBPNG_INCLUDE="${stage}"/packages/include/libpng16
OPENJPEG_INCLUDE="${stage}"/packages/include/openjpeg

[ -f "$GLIB_INCLUDE"/glib.h ] || fail "You haven't installed the glib package yet."
[ -f "$LIBPNG_INCLUDE"/png.h ] || fail "You haven't installed the libpng package yet."
[ -f "$OPENJPEG_INCLUDE"/openjpeg.h ] || fail "You haven't installed the openjpeg package yet."

# load autobuild provided shell functions and variables
source_environment_tempfile="$stage/source_environment.sh"
"$autobuild" source_environment > "$source_environment_tempfile"
. "$source_environment_tempfile"
build=${AUTOBUILD_BUILD_ID:=0}
echo "${VERSION}.${build}" > "${stage}/VERSION.txt"
case "$AUTOBUILD_PLATFORM" in
    "linux")
        # Prefer gcc-4.9 if available.
        if [[ -x /usr/bin/gcc-4.9 && -x /usr/bin/g++-4.9 ]]; then
            export CC=/usr/bin/gcc-4.9
            export CXX=/usr/bin/g++-4.9
        fi

        # Default target to 32-bit
        opts="${TARGET_OPTS:--m32}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        # Debug first
#        pushd "$TOP/$SOURCE_DIR"
#		    CFLAGS="$opts -O0 -g -fPIC -DPIC" CXXFLAGS="$opts -O0 -g -fPIC -DPIC" \
#		    ./configure --prefix="$stage" --enable-debug --includedir="$stage/include/glib" 
#		    make
#		    make install

        # conditionally run unit tests
#        if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
#            make test
#        fi
#        # clean the build artifacts
#        make distclean
#       popd 
       # Release last
        pushd "$TOP/$SOURCE_DIR"
           export PATH="$stage/packages/include":"$stage/packages/lib/release":$PATH
           LDFLAGS="-L$stage/packages/lib/releaseo" 
           CFLAGS="-I$OPENJPEG_INCLUDE -I$GLIB_INCLUDE -I$LIBPNG_INCLUDE  $opts -O3 -fPIC -DPIC"
           CC="$CC -m32" ./configure --prefix="$stage"  --without-libjpeg --without-libtiff --with-x11
           make
           make install
        popd
#copy one level up so that the viewer can find them
        mkdir -p "$stage/include/gdk-pixbuf"
	    cp -a "$stage"/include/gdk-pixbuf-2.0/gdk-pixbuf/*.h "$stage/include/gdk-pixbuf/"
        mkdir -p "$stage/include/gdk-pixbuf-xlib"
		cp -a "$stage"/include/gdk-pixbuf-2.0/gdk-pixbuf-xlib/*.h "$stage/include/gdk-pixbuf-xlib/"
        mv lib release
        mkdir -p lib
        mv release lib


    ;;
    "linux64")
        # Prefer gcc-6 if available.
        if [[ -x /usr/bin/gcc-6 && -x /usr/bin/g++-6 ]]; then
            export CC=/usr/bin/gcc-6
            export CXX=/usr/bin/g++-6
        fi

        # Default target to 64-bit
        opts="${TARGET_OPTS:--m64}"
        # Handle any deliberate platform targeting
        if [ -z "$TARGET_CPPFLAGS" ]; then
            # Remove sysroot contamination from build environment
            unset CPPFLAGS
        else
            # Incorporate special pre-processing flags
            export CPPFLAGS="$TARGET_CPPFLAGS"
        fi
        # Debug first
#        pushd "$TOP/$SOURCE_DIR"
#		    CFLAGS="$opts -O0 -g -fPIC -DPIC" CXXFLAGS="$opts -O0 -g -fPIC -DPIC" \
#		    ./configure --prefix="$stage" --enable-debug --includedir="$stage/include/glib" 
#		    make
#		    make install

        # conditionally run unit tests
#        if [ "${DISABLE_UNIT_TESTS:-0}" = "0" ]; then
#            make test
#        fi
#        # clean the build artifacts
#        make distclean
#       popd 
       # Release last
        pushd "$TOP/$SOURCE_DIR"
           export PATH="$stage/packages/include":"$stage/packages/lib/release":$PATH
           LDFLAGS="-L$stage/packages/lib/releaseo" 
           CFLAGS="-I$OPENJPEG_INCLUDE -I$GLIB_INCLUDE -I$LIBPNG_INCLUDE  $opts -O3 -fPIC -DPIC"
           CC="$CC -m64" ./configure --prefix="$stage"  --without-libjpeg --without-libtiff --with-x11
           make
           make install
        popd
#copy one level up so that the viewer can find them
        mkdir -p "$stage/include/gdk-pixbuf"
	    cp -a "$stage"/include/gdk-pixbuf-2.0/gdk-pixbuf/*.h "$stage/include/gdk-pixbuf/"
        mkdir -p "$stage/include/gdk-pixbuf-xlib"
		cp -a "$stage"/include/gdk-pixbuf-2.0/gdk-pixbuf-xlib/*.h "$stage/include/gdk-pixbuf-xlib/"
        mv lib release
        mkdir -p lib
        mv release lib
    ;;
    *)
        echo "platform not supported"
        exit -1
    ;;
esac


mkdir -p "$stage/LICENSES"
cp "$TOP/$SOURCE_DIR/$LICENSE" "$stage/LICENSES/$PROJECT.txt"


